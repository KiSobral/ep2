public class Veiculo {
    // Atributos da classe Veiculo
    private String nome;
    private byte   combustivel;
    private float  rendimento;
    private float  diminuiRendimento;
    private float  cargaMaxima;
    private float  velocidadeMedia;
    private float  precoCombustivel;

    // Definição de statics
    public static final byte DIESEL    = 1;
    public static final byte GASOLINA  = 2;
    public static final byte ALCOOL    = 3;

    // Construtor da classe Veiculo
    public Veiculo(){
        setNome("VEICULO");
        setCombustivel((byte)0);
        setRendimento(1.0f);
        setDiminuiRendimento(0.1f);
        setCargaMaxima(1.0f);
        setVelocidadeMedia(1.0f);
        setPrecoCombustivel(getCombustivel());
    }

    // Getter e Setter de nome
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    // Getter e Setter de combustivel
    public byte getCombustivel() {
        return combustivel;
    }
    protected void setCombustivel(byte combustivel) {
        this.combustivel = combustivel;
    }

    // Getter e Setter de rendimento
    public float getRendimento() {
        return rendimento;
    }
    protected void setRendimento(float rendimento) {
        this.rendimento = rendimento;
    }

    // Getter e Setter de diminuiRendimento
    public float getDiminuiRendimento() {
        return diminuiRendimento;
    }
    protected void setDiminuiRendimento(float diminuiRendimento) {
        this.diminuiRendimento = diminuiRendimento;
    }

    // Getter e Setter de cargaMaxima
    public float getCargaMaxima() {
        return cargaMaxima;
    }
    protected void setCargaMaxima(float cargaMaxima) {
        this.cargaMaxima = cargaMaxima;
    }

    // Getter e Setter de velocidadeMaxima
    public float getVelocidadeMedia() {
        return velocidadeMedia;
    }
    protected void setVelocidadeMedia(float velocidadeMedia) {
        this.velocidadeMedia = velocidadeMedia;
    }

    // Setter e Getter de precoCombustivel
    protected void setPrecoCombustivel(byte combustivel) {
        if (combustivel == DIESEL){
            this.precoCombustivel = 3.869f;

        } else if (combustivel == GASOLINA){
            this.precoCombustivel = 4.449f;

        } else if (combustivel == ALCOOL){
            this.precoCombustivel = 3.499f;

        } else {
            this.precoCombustivel = 1.0f;
        }
    }
    public float getPrecoCombustivel() {
        return precoCombustivel;
    }

    // Cálculos das entregas
    public float entregarEmTempo(float distancia){
        return distancia/velocidadeMedia;
    }

    public float entregarComMenosCusto(float distancia, float cargaTotal){
        return (distancia / (rendimento - cargaTotal*diminuiRendimento) ) * precoCombustivel;
    }

    public float entregarCustoBeneficio(float distancia, float cargaTotal){
        return entregarComMenosCusto(distancia, cargaTotal) / entregarEmTempo(distancia);
    }

    // Validações do veículo para as condições da viagem
    public boolean verificarTempo(float distancia, float tempoDeEntrega){
        if (tempoDeEntrega > distancia/velocidadeMedia){
            return true;
        } else {
            return false;
        }
    }

    public boolean verificarCarga (float cargaTotal){
        if (cargaTotal < cargaMaxima){
            return true;
        } else {
            return false;
        }
    }

}