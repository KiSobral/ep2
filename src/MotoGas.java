public class MotoGas extends Veiculo{
    // Construtor da classe MotoGas
    public MotoGas(){
        setNome("MOTO");
        setCombustivel((byte)GASOLINA);
        setRendimento(50.0f);
        setDiminuiRendimento(0.3f);
        setCargaMaxima(50.0f);
        setVelocidadeMedia(110.0f);
        setPrecoCombustivel(getCombustivel());
    }

}