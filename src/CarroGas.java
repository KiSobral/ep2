import com.sun.jdi.PathSearchingVirtualMachine;

public class CarroGas extends Veiculo {
    // Construtor da classe CarroGas
    public CarroGas(){
        setNome("CARRO");
        setCombustivel((byte)GASOLINA);
        setRendimento(14.0f);
        setDiminuiRendimento(0.025f);
        setCargaMaxima(360.0f);
        setVelocidadeMedia(100.0f);
        setPrecoCombustivel(getCombustivel());
    }

}
