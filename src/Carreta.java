public class Carreta extends Veiculo {
    // Construtor da classe Carreta
    public Carreta(){
        setNome("CARRETA");
        setCombustivel((byte)DIESEL);
        setRendimento(8.0f);
        setDiminuiRendimento(0.0002f);
        setCargaMaxima(30000.0f);
        setVelocidadeMedia(60.0f);
        setPrecoCombustivel(getCombustivel());
    }

}
