public class CarroAlc extends Veiculo {
    // Construtor da classe CarroAlc
    public CarroAlc(){
        setNome("CARRO");
        setCombustivel((byte)ALCOOL);
        setRendimento(12.0f);
        setDiminuiRendimento(0.0231f);
        setCargaMaxima(360.0f);
        setVelocidadeMedia(100.0f);
        setPrecoCombustivel(getCombustivel());
    }

}
