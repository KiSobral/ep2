import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.BorderLayout;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;


public class TransportadoraGUI {
    // Declaração do frame principal
    private JFrame mainFrame;

    // Declaração do painel principal
    private JPanel pnlControla;

    // Declaração de text fields
    private JTextField txtCarga;
    private JTextField txtDistancia;
    private JTextField txtTempo;

    private JPanel pnlBarraSuperior;
    private JPanel pnlBarraInferior;

    // Declaração de JTextFields
    JTextField txtLucro;

    // Declaração de veículos
    private Carreta  carreta = new Carreta();
    private Van      van     = new Van();
    private CarroGas carroG  = new CarroGas();
    private CarroAlc carroA  = new CarroAlc();
    private MotoGas  motoG   = new MotoGas();
    private MotoAlc  motoA   = new MotoAlc();

    // Declaração de variáveis numéricas
    private float margemDeLucro;
    private float cargaDeEntrega     = 0.0f;
    private float distanciaDeEntrega = 0.0f;
    private int   tempoDeEntrega     = 0;

    // Declaração de variáveis para armazenamento
    private Veiculo menorTempo;
    private Veiculo menorCusto;
    private Veiculo melhorCustoBeneficio;


    public TransportadoraGUI(){

        preparaBarraSuperior();
        preparaBarraInferior();
        preparaHome();
        preparaFrame();

    }

    private void preparaFrame(){

        // Atualiza o valor da margem de lucro
        margemDeLucro = ControleDeMenus.verificarMargemDeLucro();

        // Configurações do mainFrame
        mainFrame = new JFrame();
        mainFrame.setLayout(new CardLayout());
        mainFrame.setSize(700, 900);
        mainFrame.setLocation(150, 50);
        mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowsEvent){
                System.exit(0);
            }
        });
        mainFrame.add(pnlControla);
        mainFrame.setVisible(true);

        // Configurando as barras de opções
        preparaBarraSuperior();
        preparaBarraInferior();

    }

    // Páginas da home
    private void preparaHome(){

        // Declaração de painel
        JPanel pnlCentral   = new JPanel (new GridLayout(10, 1));

        // Declaração de labels
        JLabel lblTituloHome = new JLabel  ("REALIZAR ENTREGA",                JLabel.CENTER);
        JLabel lblLinha      = new JLabel  ("",                                JLabel.CENTER);
        JLabel lblCarga      = new JLabel  ("Defina a carga, em KG",           JLabel.CENTER);
        JLabel lblDistancia  = new JLabel  ("Defina a distância, em KM",       JLabel.CENTER);
        JLabel lblTempo      = new JLabel  ("Defina o tempo, em horas",        JLabel.CENTER);

        // Instancia os TextFields
        txtCarga     = new JTextField  (JTextField.CENTER);
        txtDistancia = new JTextField  (JTextField.CENTER);
        txtTempo     = new JTextField  (JTextField.CENTER);

        // Configuração do botão
        JButton btnConfirma     = new JButton("Confirmar");
        btnConfirma.setActionCommand  ("CONFIRMA");
        btnConfirma.addActionListener (new PaginaHomeListener());

        // Configuração do painél central
        pnlCentral.add(lblTituloHome);

        pnlCentral.add(lblCarga);
        pnlCentral.add(txtCarga);

        pnlCentral.add(lblDistancia);
        pnlCentral.add(txtDistancia);

        pnlCentral.add(lblTempo);
        pnlCentral.add(txtTempo);

        pnlCentral.add(lblLinha);

        pnlCentral.add(btnConfirma);

        // Configura o painél pnlControla
        pnlControla = new JPanel(new BorderLayout());
        pnlControla.add(pnlCentral,       BorderLayout.CENTER);

        pnlControla.add(pnlBarraSuperior, BorderLayout.NORTH);

        preparaBarraInferior();
        pnlControla.add(pnlBarraInferior, BorderLayout.SOUTH);

    }
    private void preparaHomeConfirmacao(Veiculo tempo, Veiculo preco, Veiculo custo){

        // Painéis
        JPanel pnlCentral       = new JPanel(new GridLayout(8, 1));
        JPanel pnlTempo         = new JPanel(new GridLayout(3,1));
        JPanel pnlPreco         = new JPanel(new GridLayout(3, 1));
        JPanel pnlCusto         = new JPanel(new GridLayout(3, 1));
        JPanel pnlBotoes        = new JPanel(new FlowLayout());

        // Setando espaços em branco
        JLabel lblBranco1       = new JLabel("", JLabel.CENTER);
        JLabel lblBranco2       = new JLabel("", JLabel.CENTER);
        JLabel lblBranco3       = new JLabel("", JLabel.CENTER);

        // Declaração da string para concatenação
        String stringPreco;

        // Setando a opção com menor tempo
        JLabel lblTempo         = new JLabel("Melhor tempo", JLabel.CENTER);
        JLabel lblCarroTempo    = new JLabel(tempo.getNome(),     JLabel.CENTER);
        stringPreco = "R$ " + Float.toString(ControleDeMenus.calcularCustoCliente(tempo, distanciaDeEntrega, cargaDeEntrega, margemDeLucro));
        JLabel lblPrecoTempo    = new JLabel(stringPreco, JLabel.CENTER);


        pnlTempo.add(lblTempo);
        pnlTempo.add(lblCarroTempo);
        pnlTempo.add(lblPrecoTempo);

        // Setando a opção com menor preço
        JLabel lblPreco         = new JLabel("Melhor preço", JLabel.CENTER);
        JLabel lblCarroPreco    = new JLabel(preco.getNome(),     JLabel.CENTER);
        stringPreco = "R$ " + Float.toString(ControleDeMenus.calcularCustoCliente(preco, distanciaDeEntrega, cargaDeEntrega, margemDeLucro));
        JLabel lblPrecoPreco    = new JLabel(stringPreco, JLabel.CENTER);

        pnlPreco.add(lblPreco);
        pnlPreco.add(lblCarroPreco);
        pnlPreco.add(lblPrecoPreco);

        // Setando a opção com melhor custo benefício
        JLabel lblCusto         = new JLabel("Melhor Custo Benefício",  JLabel.CENTER);
        JLabel lblCarroCusto    = new JLabel(custo.getNome(),                JLabel.CENTER);
        stringPreco = "R$ " + Float.toString(ControleDeMenus.calcularCustoCliente(custo, distanciaDeEntrega, cargaDeEntrega, margemDeLucro));
        JLabel lblPrecoCusto    = new JLabel(stringPreco, JLabel.CENTER);

        pnlCusto.add(lblCusto);
        pnlCusto.add(lblCarroCusto);
        pnlCusto.add(lblPrecoCusto);

        // Setando os botões
        JLabel  lblOpcoes       = new JLabel ("O cálculo da viagem deve se basear em:", JLabel.CENTER);
        JButton btnTempo        = new JButton("Tempo");
        JButton btnPreco        = new JButton("Preço");
        JButton btnCusto        = new JButton("Custo benefício");

        btnTempo.setActionCommand  ("TEMPO");
        btnPreco.setActionCommand  ("PRECO");
        btnCusto.setActionCommand  ("CUSTO");

        btnTempo.addActionListener (new PaginaHomeListener());
        btnPreco.addActionListener (new PaginaHomeListener());
        btnCusto.addActionListener (new PaginaHomeListener());

        // Configurando o painel de botões
        pnlBotoes.add(btnTempo);
        pnlBotoes.add(btnPreco);
        pnlBotoes.add(btnCusto);

        // Configurando o painel central
        pnlCentral.add(pnlTempo);
        pnlCentral.add(lblBranco1);

        pnlCentral.add(pnlPreco);
        pnlCentral.add(lblBranco2);

        pnlCentral.add(pnlCusto);
        pnlCentral.add(lblBranco3);

        pnlCentral.add(lblOpcoes);
        pnlCentral.add(pnlBotoes);

        // Configurando o pnlControla
        pnlControla = new JPanel(new BorderLayout());
        pnlControla.add(pnlCentral,       BorderLayout.CENTER);
        pnlControla.add(pnlBarraSuperior, BorderLayout.NORTH);
        pnlControla.add(pnlBarraInferior, BorderLayout.SOUTH);

    }
    private void preparaPaginaBloqueio(){

        // Label para informar a impossibilidade de operação
        JLabel lblMensagemBloqueia = new JLabel("Não é possível realizar o frete com os dados inseridos.", JLabel.CENTER);

        // Configurando o pnlControla
        pnlControla = new JPanel(new BorderLayout());
        pnlControla.add(lblMensagemBloqueia, BorderLayout.CENTER);
        pnlControla.add(pnlBarraSuperior,    BorderLayout.NORTH);
        pnlControla.add(pnlBarraInferior,    BorderLayout.SOUTH);

    }
    private void preparaPaginaConclusao(){

        // Declaração da label para informar o registro do frete
        JLabel lblMensagemConfirma = new JLabel("A viagem está sendo feita.", JLabel.CENTER);

        // Configurando o pnlControla
        pnlControla = new JPanel(new BorderLayout());
        pnlControla.add(lblMensagemConfirma, BorderLayout.CENTER);
        pnlControla.add(pnlBarraSuperior,    BorderLayout.NORTH);
        pnlControla.add(pnlBarraInferior,    BorderLayout.SOUTH);

    }

    // Páginas da frota
    private void preparaGerenciamento(){

        // Painéis da página
        JPanel pnlBarraLateral  = new JPanel(new GridLayout(2, 1));
        JPanel pnlCentral       = new JPanel(new GridLayout(6, 1));

        // Labels
        JLabel lblBranco        = new JLabel("", JLabel.CENTER);
        JLabel lblMensagem      = new JLabel("Adiciona ou remove 1 veículo", JLabel.CENTER);

        // Botões da barra lateral
        JButton btnGerenciar    = new JButton ("Gerenciar");
        btnGerenciar.setActionCommand         ("GERENCIAR");
        btnGerenciar.addActionListener        (new PaginaFrotaListener());

        JButton btnVisualizar   = new JButton ("Visualizar");
        btnVisualizar.setActionCommand        ("VISUALIZAR");
        btnVisualizar.addActionListener       (new PaginaFrotaListener());

        // Paineis para organização de visualização
        JPanel pnlOpcoesCarreta = new JPanel(new FlowLayout());
        JPanel pnlOpcoesVan     = new JPanel(new FlowLayout());
        JPanel pnlOpcoesCarro   = new JPanel(new FlowLayout());
        JPanel pnlOpcoesMoto    = new JPanel(new FlowLayout());

        // Botões da carreta
        JButton btnAddCarreta   = new JButton("Adicionar carreta");
        JButton btnRmCarreta    = new JButton("Remover carreta");

        btnAddCarreta.setActionCommand  ("ADDCAM");
        btnAddCarreta.addActionListener (new PaginaFrotaListener());
        btnRmCarreta.setActionCommand   ("RMCAM");
        btnRmCarreta.addActionListener  (new PaginaFrotaListener());

        // Botões da van
        JButton btnAddVan       = new JButton("Adiciona van");
        JButton btnRmVan        = new JButton("Remove van");

        btnAddVan.setActionCommand  ("ADDVAN");
        btnAddVan.addActionListener (new PaginaFrotaListener());
        btnRmVan.setActionCommand   ("RMVAN");
        btnRmVan.addActionListener  (new PaginaFrotaListener());

        // Botões do carro
        JButton btnAddCarro     = new JButton("Adiciona carro");
        JButton btnRmCarro      = new JButton("Remove carro");

        btnAddCarro.setActionCommand  ("ADDCAR");
        btnAddCarro.addActionListener (new PaginaFrotaListener());
        btnRmCarro.setActionCommand   ("RMCAR");
        btnRmCarro.addActionListener  (new PaginaFrotaListener());

        // Botões da moto
        JButton btnAddMoto      = new JButton("Adiciona moto");
        JButton btnRmMoto       = new JButton("Remove moto");

        btnAddMoto.setActionCommand  ("ADDMOT");
        btnAddMoto.addActionListener (new PaginaFrotaListener());
        btnRmMoto.setActionCommand   ("RMMOT");
        btnRmMoto.addActionListener  (new PaginaFrotaListener());

        // Configurações dos painéis
        // Carreta
        pnlOpcoesCarreta.add(btnAddCarreta);
        pnlOpcoesCarreta.add(btnRmCarreta);

        // Van
        pnlOpcoesVan.add(btnAddVan);
        pnlOpcoesVan.add(btnRmVan);

        // Carro
        pnlOpcoesCarro.add(btnAddCarro);
        pnlOpcoesCarro.add(btnRmCarro);

        // Moto
        pnlOpcoesMoto.add(btnAddMoto);
        pnlOpcoesMoto.add(btnRmMoto);

        // Barra lateral
        pnlBarraLateral.add(btnGerenciar);
        pnlBarraLateral.add(btnVisualizar);

        // Painel central
        pnlCentral.add(lblMensagem);
        pnlCentral.add(lblBranco);
        pnlCentral.add(pnlOpcoesCarreta);
        pnlCentral.add(pnlOpcoesVan);
        pnlCentral.add(pnlOpcoesCarro);
        pnlCentral.add(pnlOpcoesMoto);

        // Configuração do pnlControla
        pnlControla = new JPanel(new BorderLayout());
        pnlControla.add(pnlBarraSuperior,   BorderLayout.NORTH);
        pnlControla.add(pnlBarraLateral,    BorderLayout.WEST);
        pnlControla.add(pnlBarraInferior,   BorderLayout.SOUTH);
        pnlControla.add(pnlCentral,         BorderLayout.CENTER);

    }
    private void preparaVisualizacao(){

        // Painéis da página
        JPanel pnlBarraLateral  = new JPanel(new GridLayout(2, 1));
        JScrollPane pnlCentral  = new JScrollPane();

        // TextPane
        JTextPane armazenaFrota = new JTextPane();

        // Arquivo
        File arquivoDaFrota     = new File("./doc/AcervoDeVeiculos.txt");

        // Botões da barra lateral
        JButton btnGerenciar    = new JButton("Gerenciar");
        btnGerenciar.setActionCommand         ("GERENCIAR");
        btnGerenciar.addActionListener        (new PaginaFrotaListener());

        JButton btnVisualizar   = new JButton("Visualizar");
        btnVisualizar.setActionCommand        ("VISUALIZAR");
        btnVisualizar.addActionListener       (new PaginaFrotaListener());

        // Prepara barra lateral
        pnlBarraLateral.add(btnGerenciar);
        pnlBarraLateral.add(btnVisualizar);

        // Prepara o ScrollPane
        try{
            FileReader leitorPrimario = new FileReader(arquivoDaFrota);
            BufferedReader leitor     = new BufferedReader(leitorPrimario);

            armazenaFrota.read(leitor, null);

            leitor.close();
            leitorPrimario.close();

            armazenaFrota.requestFocus();
        }
        catch (Exception e){
            armazenaFrota.setText("DEU\nERRADO");
        }

        armazenaFrota.setEditable(false);

        pnlCentral.setViewportView(armazenaFrota);

        // Configuração do pnlControla
        pnlControla = new JPanel(new BorderLayout());
        pnlControla.add(pnlBarraSuperior,   BorderLayout.NORTH);
        pnlControla.add(pnlBarraLateral,    BorderLayout.WEST);
        pnlControla.add(pnlBarraInferior,   BorderLayout.SOUTH);
        pnlControla.add(pnlCentral,         BorderLayout.CENTER);

    }

    // Página do frete
    private void preparaFrete(){

        // Painel da página
        JScrollPane pnlCentral  = new JScrollPane();

        // TextPane
        JTextPane armazenaFrota = new JTextPane();

        // Arquivo
        File arquivoDaFrota     = new File("./doc/HistoricoDeViagem.txt");

        // Botões da barra lateral
        JButton btnGerenciar    = new JButton("Gerenciar");
        btnGerenciar.setActionCommand         ("GERENCIAR");
        btnGerenciar.addActionListener        (new PaginaFrotaListener());

        JButton btnVisualizar   = new JButton("Visualizar");
        btnVisualizar.setActionCommand        ("VISUALIZAR");
        btnVisualizar.addActionListener       (new PaginaFrotaListener());

        // Prepara o ScrollPane
        try{
            FileReader leitorPrimario = new FileReader(arquivoDaFrota);
            BufferedReader leitor     = new BufferedReader(leitorPrimario);

            armazenaFrota.read(leitor, null);

            leitor.close();
            leitorPrimario.close();

            armazenaFrota.requestFocus();
        }
        catch (Exception e){
            armazenaFrota.setText("DEU\nERRADO");
        }

        armazenaFrota.setEditable(false);

        pnlCentral.setViewportView(armazenaFrota);

        // Configuração do pnlControla
        pnlControla = new JPanel(new BorderLayout());
        pnlControla.add(pnlBarraSuperior,   BorderLayout.NORTH);
        pnlControla.add(pnlBarraInferior,   BorderLayout.SOUTH);
        pnlControla.add(pnlCentral,         BorderLayout.CENTER);

    }

    // Página do reset
    private void preparaPaginaReset(){

        // Label para informar o reset nos dados
        JLabel lblMensagemReset = new JLabel("Os dados foram resetados.", JLabel.CENTER);

        // Configuração do pnlControla
        pnlControla = new JPanel(new BorderLayout());
        pnlControla.add(lblMensagemReset, BorderLayout.CENTER);
        pnlControla.add(pnlBarraSuperior, BorderLayout.NORTH);
        pnlControla.add(pnlBarraInferior, BorderLayout.SOUTH);

    }

    // Páginas do Lucro
    private void preparaLucro(){
        // Declaração do painel principal
        JPanel pnlCentral        = new JPanel(new GridLayout(8, 1));

        // Declaração de Labels
        JLabel  lblBranco1       = new JLabel ("",                                    JLabel.CENTER);
        JLabel  lblMensagem      = new JLabel ("Insira a nova margem de lucro, em %", JLabel.CENTER);
        JLabel  lblBranco2       = new JLabel ("",                                    JLabel.CENTER);
        JLabel  lblBranco3       = new JLabel ("",                                    JLabel.CENTER);
        JLabel  lblBranco4       = new JLabel ("",                                    JLabel.CENTER);
        JLabel  lblBranco5       = new JLabel ("",                                    JLabel.CENTER);

        // Instancia do TextField
        txtLucro = new JTextField(JTextField.CENTER);

        // Configuração do botão de confirmar lucro
        JButton btnConfirmaLucro = new JButton ("Confirmar lucro");
        btnConfirmaLucro.setActionCommand      ("CONFLUCRO");
        btnConfirmaLucro.addActionListener     (new SuperiorInferiorListener());

        // Configurações do painel central
        pnlCentral.add(lblBranco1);
        pnlCentral.add(lblBranco2);
        pnlCentral.add(lblMensagem);
        pnlCentral.add(txtLucro);
        pnlCentral.add(lblBranco3);
        pnlCentral.add(lblBranco4);
        pnlCentral.add(btnConfirmaLucro);
        pnlCentral.add(lblBranco5);

        // Configurações do pnlControla
        pnlControla = new JPanel(new BorderLayout());
        pnlControla.add(pnlBarraSuperior,   BorderLayout.NORTH);
        pnlControla.add(pnlCentral,         BorderLayout.CENTER);
        pnlControla.add(pnlBarraInferior,   BorderLayout.SOUTH);

    }
    private void preparaMensagemLucro(){
        // Declaração da label para informar a atualização da margem de lucro
        JLabel lblMensagemLucro = new JLabel("A margem de lucro foi atualizada.", JLabel.CENTER);

        // Configurações do pnlControla
        pnlControla = new JPanel(new BorderLayout());
        pnlControla.add(lblMensagemLucro, BorderLayout.CENTER);
        pnlControla.add(pnlBarraSuperior, BorderLayout.NORTH);
        pnlControla.add(pnlBarraInferior, BorderLayout.SOUTH);

    }

    // Barras superior e inferior
    private void preparaBarraSuperior(){
        // Configurações dos botões da barra superior
        JButton btnHome   = new JButton("Página inicial");
        JButton btnFrota  = new JButton("Controle de frota");
        JButton btnFrete  = new JButton("Histórico de fretes");

        btnFrota.setActionCommand  ("FROTA");
        btnHome.setActionCommand   ("HOME");
        btnFrete.setActionCommand  ("FRETE");

        btnFrota.addActionListener (new SuperiorInferiorListener());
        btnHome.addActionListener  (new SuperiorInferiorListener());
        btnFrete.addActionListener (new SuperiorInferiorListener());

        // Configurações do painel
        pnlBarraSuperior = new JPanel();
        pnlBarraSuperior.setLayout(new FlowLayout());
        pnlBarraSuperior.add  (btnFrota);
        pnlBarraSuperior.add  (btnHome);
        pnlBarraSuperior.add  (btnFrete);
    }
    private void preparaBarraInferior(){
        // Configurações dos botões da barra inferior
        JButton btnReset  = new JButton("Resetar dados");
        JButton btnLucro  = new JButton("Definir Margem de Lucro");

        btnReset.setActionCommand("RESET");
        btnLucro.setActionCommand("LUCRO");

        btnReset.addActionListener (new SuperiorInferiorListener());
        btnLucro.addActionListener (new SuperiorInferiorListener());

        // Configurações do painel
        pnlBarraInferior = new JPanel();
        pnlBarraInferior.setLayout  (new FlowLayout());
        pnlBarraInferior.add  (btnReset);
        pnlBarraInferior.add  (btnLucro);
    }

    // Listeners
    private class SuperiorInferiorListener implements ActionListener {
        // Tratamento para os botões provenientes das barras superior e inferior
        public void actionPerformed(ActionEvent event){
            String comand = event.getActionCommand();

            if (comand.equals("HOME")) {

                mainFrame.remove(pnlControla);
                mainFrame.setVisible(false);

                preparaHome();
                preparaFrame();

            } else if (comand.equals("FROTA")) {

                mainFrame.remove(pnlControla);
                mainFrame.setVisible(false);

                preparaGerenciamento();
                preparaFrame();

            } else if (comand.equals("FRETE")) {

                mainFrame.remove(pnlControla);
                mainFrame.setVisible(false);

                preparaFrete();
                preparaFrame();

            } else if (comand.equals("RESET")) {

                ControleDeMenus.resetarDados();

                mainFrame.remove(pnlControla);
                mainFrame.setVisible(false);

                preparaPaginaReset();
                preparaFrame();

            } else if (comand.equals("LUCRO")) {

                mainFrame.remove(pnlControla);
                mainFrame.setVisible(false);

                preparaLucro();
                preparaFrame();

            } else if (comand.equals("CONFLUCRO")){

                margemDeLucro = Float.parseFloat(txtLucro.getText());
                margemDeLucro = margemDeLucro / 100;
                ControleDeMenus.atualizarMargemDeLucro(margemDeLucro);

                mainFrame.remove(pnlControla);
                mainFrame.setVisible(false);

                preparaMensagemLucro();
                preparaFrame();

            }
        }
    }

    private class PaginaHomeListener implements ActionListener {
        // Tratamento para os botões provenientes da página home
        public void actionPerformed(ActionEvent event) {
            String comand = event.getActionCommand();

            if (comand.equals("CONFIRMA")){

                // Declaração do boolean para validação da viagem
                boolean veiculoCapaz = false;

                // Declaração de booleans para validação da carreta
                boolean carretaCarga;
                boolean carretaTempo;
                boolean carretaDisponivel;
                boolean carretaValidacao = false;

                // Declaração de booleans para validação da van
                boolean vanCarga;
                boolean vanTempo;
                boolean vanDisponivel;
                boolean vanValidacao = false;

                // Declaração de booleans para validação do carro
                boolean carroCarga;
                boolean carroTempo;
                boolean carroDisponivel;
                boolean carroValidacao = false;

                // Declaração de booleans para validação da moto
                boolean motoCarga;
                boolean motoTempo;
                boolean motoDisponivel;
                boolean motoValidacao = false;


                cargaDeEntrega      = Float.parseFloat(txtCarga.getText());
                distanciaDeEntrega  = Float.parseFloat(txtDistancia.getText());
                tempoDeEntrega      = Integer.parseInt(txtTempo.getText());

                // Validação da carreta
                carretaCarga        = carreta.verificarCarga(cargaDeEntrega);
                carretaTempo        = carreta.verificarTempo(distanciaDeEntrega, tempoDeEntrega);
                carretaDisponivel   = ControleDeMenus.verificarVeiculo(carreta.getNome());
                if (carretaCarga && carretaTempo && carretaDisponivel) {

                    carretaValidacao = true;
                }

                // Validação da van
                vanCarga            = van.verificarCarga(cargaDeEntrega);
                vanTempo            = van.verificarTempo(distanciaDeEntrega, tempoDeEntrega);
                vanDisponivel       = ControleDeMenus.verificarVeiculo(van.getNome());
                if (vanCarga && vanTempo && vanDisponivel) {

                    vanValidacao = true;
                }

                // Validação do carro
                carroCarga      = carroG.verificarCarga(cargaDeEntrega);
                carroTempo      = carroA.verificarTempo(distanciaDeEntrega, tempoDeEntrega);
                carroDisponivel = ControleDeMenus.verificarVeiculo(carroG.getNome());
                if (carroCarga && carroTempo && carroDisponivel) {

                    carroValidacao = true;
                }

                // Validação da moto
                motoCarga       = motoA.verificarCarga(cargaDeEntrega);
                motoTempo       = motoG.verificarTempo(distanciaDeEntrega, tempoDeEntrega);
                motoDisponivel  = ControleDeMenus.verificarVeiculo(motoA.getNome());
                if (motoCarga && motoTempo && motoDisponivel) {

                    motoValidacao = true;

                }

                // Verificação se existem veículos disponíveis
                if (carretaValidacao || vanValidacao || carroValidacao || motoValidacao){

                    veiculoCapaz = true;
                }

                // Sort na carreta
                if (carretaValidacao){
                    menorTempo           = new Carreta();
                    menorCusto           = new Carreta();
                    melhorCustoBeneficio = new Carreta();
                }

                // Sort na van
                if (vanValidacao){
                    // Definição inicial
                    if (!carretaValidacao){
                        menorTempo           = new Van();
                        menorCusto           = new Van();
                        melhorCustoBeneficio = new Van();
                    } else {

                        if (van.entregarEmTempo(distanciaDeEntrega) < menorTempo.entregarEmTempo(distanciaDeEntrega)){
                            menorTempo  = new Van();
                        }

                        if (van.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega) < menorCusto.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega)){
                            menorCusto  = new Van();
                        }

                        if (van.entregarCustoBeneficio(distanciaDeEntrega, cargaDeEntrega) < melhorCustoBeneficio.entregarCustoBeneficio(distanciaDeEntrega, cargaDeEntrega)){
                            melhorCustoBeneficio = new Van();
                        }

                    }
                }

                // Sort no carro
                if (carroValidacao){
                    // Definição inicial
                    if (!carretaValidacao && !vanValidacao){
                        menorTempo           = new CarroGas();
                        menorCusto           = new CarroGas();
                        melhorCustoBeneficio = new CarroGas();
                    }

                    // Carro usando gasolina
                    if (carroG.entregarEmTempo(distanciaDeEntrega) < menorTempo.entregarEmTempo(distanciaDeEntrega)){
                        menorTempo           = new CarroGas();
                    }
                    if (carroG.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega) < menorCusto.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega)){
                        menorCusto           = new CarroGas();
                    }
                    if (carroG.entregarCustoBeneficio(distanciaDeEntrega, cargaDeEntrega) < melhorCustoBeneficio.entregarCustoBeneficio(distanciaDeEntrega, cargaDeEntrega)){
                        melhorCustoBeneficio = new CarroGas();
                    }

                    // Carro usando alcool
                    if (carroA.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega) < menorCusto.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega)){

                        menorTempo           = new CarroAlc();
                        menorCusto           = new CarroAlc();

                    }
                    if (carroA.entregarCustoBeneficio(distanciaDeEntrega, cargaDeEntrega) < melhorCustoBeneficio.entregarCustoBeneficio(distanciaDeEntrega, cargaDeEntrega)){
                        melhorCustoBeneficio = new CarroAlc();
                    }
                }

                // Sort na moto
                if (motoValidacao){
                    if (!carretaValidacao && !vanValidacao && !carroValidacao){
                        menorTempo           = new MotoGas();
                        menorCusto           = new MotoGas();
                        melhorCustoBeneficio = new MotoGas();
                    }

                    // Moto usando gasolina
                    if (motoG.entregarEmTempo(distanciaDeEntrega) < menorTempo.entregarEmTempo(distanciaDeEntrega)){
                        menorTempo           = new MotoGas();
                    }
                    if (motoG.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega) < menorCusto.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega)){
                        menorCusto           = new MotoGas();
                    }
                    if (motoG.entregarCustoBeneficio(distanciaDeEntrega, cargaDeEntrega) < melhorCustoBeneficio.entregarCustoBeneficio(distanciaDeEntrega, cargaDeEntrega)){
                        melhorCustoBeneficio = new MotoGas();
                    }

                    // Moto usando alcool
                    if (motoA.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega) < menorCusto.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega)){

                        menorTempo           = new MotoAlc();
                        menorCusto           = new MotoAlc();

                    }
                    if (motoA.entregarCustoBeneficio(distanciaDeEntrega, cargaDeEntrega) < melhorCustoBeneficio.entregarCustoBeneficio(distanciaDeEntrega, cargaDeEntrega)){
                        melhorCustoBeneficio = new MotoAlc();
                    }
                }

                mainFrame.remove(pnlControla);
                mainFrame.setVisible(false);

                if (veiculoCapaz){

                    preparaHomeConfirmacao(menorTempo, menorCusto, melhorCustoBeneficio);
                    preparaFrame();

                } else {
                    preparaPaginaBloqueio();
                    preparaFrame();
                }

            } else if (comand.equals("TEMPO")) {

                ControleDeMenus.registrarEntrega(menorTempo.getNome(), menorTempo.getCombustivel(), cargaDeEntrega, distanciaDeEntrega, menorTempo.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega), ControleDeMenus.calcularCustoCliente(menorTempo, distanciaDeEntrega, cargaDeEntrega, margemDeLucro));
                ControleDeMenus.atualizarDisponibilidadeVeiculos(menorTempo.getNome(), ControleDeMenus.VIAGEM);

                mainFrame.remove(pnlControla);
                mainFrame.setVisible(false);

                preparaPaginaConclusao();
                preparaFrame();

            } else if (comand.equals("PRECO")) {

                ControleDeMenus.registrarEntrega(menorCusto.getNome(), menorCusto.getCombustivel(), cargaDeEntrega, distanciaDeEntrega, menorCusto.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega), ControleDeMenus.calcularCustoCliente(menorCusto, distanciaDeEntrega, cargaDeEntrega, margemDeLucro));
                ControleDeMenus.atualizarDisponibilidadeVeiculos(menorCusto.getNome(), ControleDeMenus.VIAGEM);

                mainFrame.remove(pnlControla);
                mainFrame.setVisible(false);

                preparaPaginaConclusao();
                preparaFrame();

            } else if (comand.equals("CUSTO")) {

                ControleDeMenus.registrarEntrega(melhorCustoBeneficio.getNome(), melhorCustoBeneficio.getCombustivel(), cargaDeEntrega, distanciaDeEntrega, melhorCustoBeneficio.entregarComMenosCusto(distanciaDeEntrega, cargaDeEntrega), ControleDeMenus.calcularCustoCliente(melhorCustoBeneficio, distanciaDeEntrega, cargaDeEntrega, margemDeLucro));
                ControleDeMenus.atualizarDisponibilidadeVeiculos(melhorCustoBeneficio.getNome(), ControleDeMenus.VIAGEM);

                mainFrame.remove(pnlControla);
                mainFrame.setVisible(false);

                preparaPaginaConclusao();
                preparaFrame();

            }
        }
    }

    private class PaginaFrotaListener implements ActionListener {
        // Tratamento dos botões provenientes da página de frota
        public void actionPerformed (ActionEvent event) {
            String comand = event.getActionCommand();

            if (comand.equals("VISUALIZAR")) {

                mainFrame.remove(pnlControla);
                mainFrame.setVisible(false);

                preparaVisualizacao();
                preparaFrame();

            } else if (comand.equals("GERENCIAR")) {

                mainFrame.remove(pnlControla);
                mainFrame.setVisible(false);

                preparaGerenciamento();
                preparaFrame();

            } else if (comand.equals("ADDCAM")) {
                ControleDeMenus.atualizarQuantidadeVeiculos("CARRETA", 1, ControleDeMenus.INCREMENTA);

            } else if (comand.equals("RMCAM")) {
                ControleDeMenus.atualizarQuantidadeVeiculos("CARRETA", 1, ControleDeMenus.DECREMENTA);

            } else if (comand.equals("ADDVAN")) {
                ControleDeMenus.atualizarQuantidadeVeiculos("VAN", 1, ControleDeMenus.INCREMENTA);

            } else if (comand.equals("RMVAN")) {
                ControleDeMenus.atualizarQuantidadeVeiculos("VAN", 1, ControleDeMenus.DECREMENTA);

            } else if (comand.equals("ADDCAR")) {
                ControleDeMenus.atualizarQuantidadeVeiculos("CARRO", 1, ControleDeMenus.INCREMENTA);

            } else if (comand.equals("RMCAR")){
                ControleDeMenus.atualizarQuantidadeVeiculos("CARRO", 1, ControleDeMenus.DECREMENTA);

            } else if (comand.equals("ADDMOT")) {
                ControleDeMenus.atualizarQuantidadeVeiculos("MOTO", 1, ControleDeMenus.INCREMENTA);

            } else if (comand.equals("RMMOT")){
                ControleDeMenus.atualizarQuantidadeVeiculos("MOTO", 1, ControleDeMenus.DECREMENTA);

            }
        }
    }
}
