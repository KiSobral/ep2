import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.BufferedReader;

public class ControleDeMenus {
    // Declaração de constants
    public static final boolean INCREMENTA = true;     // Constant usada para incrementar a quantidade de veiculos
    public static final boolean DECREMENTA = false;    // Constant usada para decrementar a quantidade de veiculos

    public static final boolean GARAGEM    = true;     // Constant usada para definir tornar o veiculo disponível
    public static final boolean VIAGEM     = false;    // Constant usada para enviar o veículo a uma viagem


    // Método que atualiza a quantidade de veículos no arquivo
    public static int atualizarQuantidadeVeiculos(String nomeDoVeiculo, int quantidadeVeiculo, boolean selecionaVersao){
        String   armazenaLinhas;                // String usada para armazenar as linhas do arquivo
        String[] armazenaSplit;                 // String usada para armazenar o split das linhas do arquivo
        int      armazenaQuantidade;            // Variável usada para armazenar e manipular valores numéricos do arquivo
        boolean  condicaoDeEscrita = false;     // Variável usada como flag para atualizar a quantidade de veiculos

        File arquivoPrincipal  = new File("./doc/AcervoDeVeiculos.txt");
        File arquivoTemporario = new File("./doc/TempFile.txt");

        // Objetos de stream de escrita
        FileWriter  escritorPrimario;
        PrintWriter escritor;

        // Objetos de stream de entrada
        FileReader      leitorPrimario;
        BufferedReader  leitor;

        try{
            // Instanciamento dos arquivos
            escritorPrimario = new FileWriter(arquivoTemporario);
            escritor         = new PrintWriter(escritorPrimario);
            leitorPrimario   = new FileReader(arquivoPrincipal);
            leitor           = new BufferedReader(leitorPrimario);

            // Lendo a primeira linha e convertendo a informação em um dado INT
            armazenaLinhas     = leitor.readLine();
            armazenaSplit      = armazenaLinhas.split(" ");
            armazenaQuantidade = Integer.parseInt(armazenaSplit[1]);

            // Incrementando ou decrementando a quantidade de veículos
            if (selecionaVersao == INCREMENTA){
                armazenaQuantidade = armazenaQuantidade + quantidadeVeiculo;
            } else if (selecionaVersao == DECREMENTA){
                armazenaQuantidade = armazenaQuantidade - quantidadeVeiculo;
            }

            // Armazenando no arquivo temporário a informação atualizada
            escritor.println(armazenaSplit[0] + " " + armazenaQuantidade);

            // Percorrendo o arquivo inteiro
            while ((armazenaLinhas = leitor.readLine()) != null){
                if (!condicaoDeEscrita){
                    armazenaSplit = armazenaLinhas.split(" ");
                    escritor.println(armazenaLinhas);

                    // Condição para a atualização do número de veiculos
                    if ( armazenaSplit[1].compareTo(nomeDoVeiculo)==0 ){
                        condicaoDeEscrita = true;
                    }

                } else {

                    armazenaSplit      = armazenaLinhas.split(" ");
                    armazenaQuantidade = Integer.parseInt(armazenaSplit[1]);

                    if (selecionaVersao == INCREMENTA){
                        armazenaQuantidade = armazenaQuantidade + quantidadeVeiculo;

                    } else if (selecionaVersao == DECREMENTA){
                        armazenaQuantidade = armazenaQuantidade - quantidadeVeiculo;

                        if (armazenaQuantidade<0) {
                            throw new Exception();
                        }
                    }

                    escritor.println(armazenaSplit[0] + " " + armazenaQuantidade);
                    condicaoDeEscrita = false;
                }
            }

            // Fechando as streams
            escritor.close();
            escritorPrimario.close();
            leitor.close();
            leitorPrimario.close();

            // Instanciamento dos arquivos
            escritorPrimario = new FileWriter(arquivoPrincipal);
            escritor         = new PrintWriter(escritorPrimario);
            leitorPrimario   = new FileReader(arquivoTemporario);
            leitor           = new BufferedReader(leitorPrimario);

            // Atualizando os dados no arquivo principal
            while ((armazenaLinhas=leitor.readLine()) != null) escritor.println(armazenaLinhas);

            // Fechando as streams
            escritor.close();
            escritorPrimario.close();
            leitor.close();
            leitorPrimario.close();

            // Limpando o arquivo temporário
            escritorPrimario = new FileWriter(arquivoTemporario);
            escritorPrimario.write("");
            escritorPrimario.close();

        }
        catch (Exception e){
            // Limpando o arquivo temporário
            try{
                escritorPrimario = new FileWriter(arquivoTemporario);
                escritorPrimario.write("");
                escritorPrimario.close();
            }
            catch (Exception e1){
                return 1;
            }

            return 1;
        }

        return 0;
    }

    // Método para atualizar a disponibilidade do veículo
    public static int atualizarDisponibilidadeVeiculos(String nomeDoVeiculo, boolean estadoDoVeiculo){
        String   armazenaLinhas;                // String usada para armazenar as linhas do arquivo
        String[] armazenaSplit;                 // String usada para armazenar o split das linhas do arquivo
        int      armazenaQuantidade;            // Variável usada para armazenar e manipular valores numéricos do arquivo
        boolean  condicaoDeGaragem = false;     // Variável usada como flag para indicar a linha da quantidade de veículos em garagem
        boolean  condicaoDeViagem  = false;     // Variável usada como flag para indicar a linha da quantidade de veículos em viagem

        File arquivoPrincipal  = new File("./doc/AcervoDeVeiculos.txt");
        File arquivoTemporario = new File("./doc/TempFile.txt");

        // Objetos de stream de escrita
        FileWriter  escritorPrimario;
        PrintWriter escritor;

        // Objetos de stream de entrada
        FileReader      leitorPrimario;
        BufferedReader  leitor;

        try{
            // Instanciamento dos arquivos
            escritorPrimario = new FileWriter(arquivoTemporario);
            escritor         = new PrintWriter(escritorPrimario);
            leitorPrimario   = new FileReader(arquivoPrincipal);
            leitor           = new BufferedReader(leitorPrimario);

            // Percorrendo o arquivo inteiro
            while ((armazenaLinhas = leitor.readLine()) != null){
                if (!condicaoDeGaragem && !condicaoDeViagem){

                    armazenaSplit = armazenaLinhas.split(" ");
                    escritor.println(armazenaLinhas);

                    if (nomeDoVeiculo.compareTo(armazenaSplit[1]) == 0){
                        condicaoDeGaragem = true;
                    }

                // Condição para a linha de veículos disponíveis
                } else if (condicaoDeGaragem && !condicaoDeViagem){

                    armazenaSplit      = armazenaLinhas.split(" ");
                    armazenaQuantidade = Integer.parseInt(armazenaSplit[1]);

                    // Lançando exceção caso haja algum erro
                    if (armazenaQuantidade <= 0) throw new Exception();

                    // Incrementando ou decrementando a quantidade de veiculos disponiveis
                    if (estadoDoVeiculo == GARAGEM) {
                        armazenaQuantidade++;
                    } else if (estadoDoVeiculo == VIAGEM){
                        armazenaQuantidade--;
                    }

                    escritor.println(armazenaSplit[0] + " " + armazenaQuantidade);
                    condicaoDeViagem = true;

                // Condição para a linha de veículos em viagem
                } else if (condicaoDeGaragem && condicaoDeViagem){

                    armazenaSplit      = armazenaLinhas.split(" ");
                    armazenaQuantidade = Integer.parseInt(armazenaSplit[1]);

                    // Incrementando ou decrementando a quantidade de veiculos disponiveis
                    if (estadoDoVeiculo == GARAGEM) {
                        armazenaQuantidade--;
                    } else if (estadoDoVeiculo == VIAGEM){
                        armazenaQuantidade++;
                    }

                    // Lançando exceção caso haja algum erro
                    if (armazenaQuantidade < 0) throw new Exception();

                    escritor.println(armazenaSplit[0] + " " + armazenaQuantidade);

                    condicaoDeGaragem = false;
                    condicaoDeViagem  = false;
                }
            }

            // Fechando as streams
            escritor.close();
            escritorPrimario.close();
            leitor.close();
            leitorPrimario.close();

            // Instanciamento dos arquivos
            escritorPrimario = new FileWriter(arquivoPrincipal);
            escritor         = new PrintWriter(escritorPrimario);
            leitorPrimario   = new FileReader(arquivoTemporario);
            leitor           = new BufferedReader(leitorPrimario);

            // Atualizando os dados no arquivo principal
            while ((armazenaLinhas=leitor.readLine()) != null) escritor.println(armazenaLinhas);

            // Fechando as streams
            escritor.close();
            escritorPrimario.close();
            leitor.close();
            leitorPrimario.close();

            // Limpando o arquivo temporário
            escritorPrimario = new FileWriter(arquivoTemporario);
            escritorPrimario.write("");
            escritorPrimario.close();

        }
        catch (Exception e){
            // Limpando o arquivo temporário
            try{
                escritorPrimario = new FileWriter(arquivoTemporario);
                escritorPrimario.write("");
                escritorPrimario.close();
            }
            catch (Exception e1){
                return 1;
            }

            return 1;
        }

        return 0;
    }

    // Método que atualiza a margem de lucro do programa
    public static int atualizarMargemDeLucro(float novaMargemDeLucro){
        String      armazenaLinhas;     // String usada para armazenar as linhas do arquivo
        String[]    armazenaSplit;      // String usada para armazenar o split das linhas do arquivo

        File arquivoPrincipal  = new File("./doc/HistoricoDeViagem.txt");
        File arquivoTemporario = new File("./doc/TempFile.txt");

        // Objetos de stream de escrita
        FileWriter  escritorPrimario;
        PrintWriter escritor;

        // Objetos de stream de entrada
        FileReader      leitorPrimario;
        BufferedReader  leitor;

        try{
            // Instanciamento dos arquivos em streams de escrita e saída
            escritorPrimario = new FileWriter(arquivoTemporario);
            escritor         = new PrintWriter(escritorPrimario);
            leitorPrimario   = new FileReader(arquivoPrincipal);
            leitor           = new BufferedReader(leitorPrimario);

            // Atualizando o valor da margem de lucro
            armazenaLinhas = leitor.readLine();
            armazenaSplit  = armazenaLinhas.split(" ");
            escritor.println(armazenaSplit[0] + " " + novaMargemDeLucro);

            while ((armazenaLinhas = leitor.readLine()) != null) escritor.println(armazenaLinhas);

            // Fechando as streams
            escritor.close();
            escritorPrimario.close();
            leitor.close();
            leitorPrimario.close();

            // Instanciamento dos arquivos
            escritorPrimario = new FileWriter(arquivoPrincipal);
            escritor         = new PrintWriter(escritorPrimario);
            leitorPrimario   = new FileReader(arquivoTemporario);
            leitor           = new BufferedReader(leitorPrimario);

            // Atualizando os dados no arquivo principal
            while ((armazenaLinhas=leitor.readLine()) != null) escritor.println(armazenaLinhas);

            // Fechando as streams
            escritor.close();
            escritorPrimario.close();
            leitor.close();
            leitorPrimario.close();

            // Limpando o arquivo temporário
            escritorPrimario = new FileWriter(arquivoTemporario);
            escritorPrimario.write("");
            escritorPrimario.close();

        }
        catch (Exception e){
            return 1;
        }

        return 0;
    }

    // Método que retorna a margem de lucro cadastrada
    public static float verificarMargemDeLucro(){
        String   armazenaLinhas;
        String[] armazenaSplit;
        float    armazenaLucro = 0.0f;

        File arquivoPrincipal  = new File("./doc/HistoricoDeViagem.txt");


        // Objetos de stream de entrada
        FileReader      leitorPrimario;
        BufferedReader  leitor;

        try{
            // Instanciamento dos arquivos em streams de escrita e saída
            leitorPrimario   = new FileReader(arquivoPrincipal);
            leitor           = new BufferedReader(leitorPrimario);

            // Lendo a primeira linha e atualizando os valores
            armazenaLinhas   = leitor.readLine();
            armazenaSplit    = armazenaLinhas.split(" ");
            armazenaLucro    = Float.parseFloat(armazenaSplit[1]);

            //Fechando a stream
            leitor.close();
            leitorPrimario.close();
        }
        catch (Exception e){
            return -1.0f;
        }

        return armazenaLucro;
    }

    // Método que registra a entrega realizada
    public static int registrarEntrega (String nomeDoVeiculo, byte combustivel, float carga, float distancia, float custo, float preco){
        String   nomeDoCombustivel ;         // String usada para designar o combustível utilizado na viagem
        String   armazenaLinhas;            // String usada para armazenar as linhas dos arquivos
        String[] armazenaSplit;             // String usada para armazenar o split das linhas do arquivo
        int      armazenaEntregas;          // Variável usada para armazenar e manipular a quantidade de entregas realizadas;
        float    armazenaLucro;             // Variável usada para armazenar e manipular a quantidade de dinheiro ganha pela empresa

        // Definição do nome da String nomeDoCombustivel
        if      (combustivel == Veiculo.DIESEL)    nomeDoCombustivel = "DIE";
        else if (combustivel == Veiculo.GASOLINA)  nomeDoCombustivel = "GAS";
        else if (combustivel == Veiculo.ALCOOL)    nomeDoCombustivel = "ALC";
        else                                       nomeDoCombustivel = "NDA";

        // Declaração dos arquivos usados na função
        File arquivoPrincipal  = new File("./doc/HistoricoDeViagem.txt");
        File arquivoTemporario = new File("./doc/TempFile.txt");

        // Objetos de stream de escrita
        FileWriter  escritorPrimario;
        PrintWriter escritor;

        // Objetos de stream de entrada
        FileReader      leitorPrimario;
        BufferedReader  leitor;

        try {
            // Instanciamento dos arquivos em streams de escrita e saída
            escritorPrimario = new FileWriter(arquivoTemporario);
            escritor         = new PrintWriter(escritorPrimario);
            leitorPrimario   = new FileReader(arquivoPrincipal);
            leitor           = new BufferedReader(leitorPrimario);

            // Lendo a primeira linha e transcrevendo ao arquivo temporário
            armazenaLinhas = leitor.readLine();
            escritor.println(armazenaLinhas);

            // Lendo a segunda linha e atualizando os valores
            armazenaLinhas   = leitor.readLine();
            armazenaSplit    = armazenaLinhas.split(" ");
            armazenaEntregas = Integer.parseInt(armazenaSplit[1]);
            armazenaEntregas++;
            escritor.println(armazenaSplit[0] + " " + armazenaEntregas);

            // Lendo a terceira linha e atualizando os valores
            armazenaLinhas = leitor.readLine();
            armazenaSplit  = armazenaLinhas.split(" ");
            armazenaLucro  = Float.parseFloat(armazenaSplit[1]);
            armazenaLucro  = armazenaLucro + (preco - custo);
            escritor.println(armazenaSplit[0] + " " + armazenaLucro);

            // Percorrendo o arquivo inteiro
            while ((armazenaLinhas = leitor.readLine()) != null) escritor.println(armazenaLinhas);

            // Fechando as streams
            escritor.close();
            escritorPrimario.close();
            leitor.close();
            leitorPrimario.close();

            // Instanciamento dos arquivos
            escritorPrimario = new FileWriter(arquivoPrincipal);
            escritor         = new PrintWriter(escritorPrimario);
            leitorPrimario   = new FileReader(arquivoTemporario);
            leitor           = new BufferedReader(leitorPrimario);

            // Atualizando os dados no arquivo principal
            while ((armazenaLinhas=leitor.readLine()) != null) escritor.println(armazenaLinhas);

            // Fechando as streams
            escritor.close();
            escritorPrimario.close();
            leitor.close();
            leitorPrimario.close();

            // Intanciando as streams de escrita
            escritorPrimario = new FileWriter(arquivoPrincipal, true);
            escritor         = new PrintWriter(escritorPrimario);

            // Adicionando uma nova entrega
            escritor.println  ("ID "            + armazenaEntregas);
            escritor.println  ("Veiculo: "      + nomeDoVeiculo);
            escritor.println  ("Combustível: "  + nomeDoCombustivel);
            escritor.println  ("Carga: "        + carga);
            escritor.println  ("Distancia: "    + distancia);
            escritor.println  ("Custo: "        + custo);
            escritor.println  ("Preço: "        + preco);
            escritor.println  ("#--#--#--#--#--#--#--#--#--#--#");

            // Fechando as streams
            escritor.close();
            escritorPrimario.close();

            // Limpando o arquivo temporário
            escritorPrimario = new FileWriter(arquivoTemporario);
            escritorPrimario.write("");
            escritorPrimario.close();

        }
        catch (Exception e){
            return 1;
        }

        return 0;
    }


    // Método que verifica a existência de veículos
    public static boolean verificarVeiculo (String nomeDoveiculo){
        String   armazenaLinhas;                    // String usada para armazenar as linhas do arquivo
        String[] armazenaSplit;                     // String usada para armazenar o split das linhas do arquivo
        int      armazenaQuantidade;                // Variável usada para armazenar e manipular valores numéricos do arquivo
        boolean  condicaoDeVerificacao = false;     // Variável usada como flag para identificar o veiculo

        // Declaração e instância do arquivo
        File arquivoPrincipal  = new File("./doc/AcervoDeVeiculos.txt");

        // Objetos de stream de entrada
        FileReader leitorPrimario;
        BufferedReader leitor;

        // Bloco Try Catch para manipulação de arquivos
        try{
            // Instância dos buffers de stream de entrada
            leitorPrimario = new FileReader(arquivoPrincipal);
            leitor         = new BufferedReader(leitorPrimario);

            // Leitura e armazenamento da primeira linha do arquivo
            armazenaLinhas     = leitor.readLine();
            armazenaSplit      = armazenaLinhas.split(" ");
            armazenaQuantidade = Integer.parseInt(armazenaSplit[1]);

            // Verificação da quantidade de veículos cadastrados
            if (armazenaQuantidade==0){
                throw new Exception();
            }

            // Percorrendo o arquivo inteiro
            while ((armazenaLinhas = leitor.readLine()) != null){
                armazenaSplit = armazenaLinhas.split(" ");

                if (!condicaoDeVerificacao){
                    if (nomeDoveiculo.compareTo(armazenaSplit[1]) == 0){
                        condicaoDeVerificacao = true;
                    }
                } else {
                    armazenaQuantidade = Integer.parseInt(armazenaSplit[1]);

                    if (armazenaQuantidade == 0){
                        throw new Exception();
                    }

                    condicaoDeVerificacao = false;
                }
            }
        }
        catch (Exception e){
            // Retorna falso caso não existam veículos disponíveis
            return false;
        }

        // Retorna verdadeiro caso existam veículos disponíveis
        return true;
    }



    // Método que retorna o custo, em reais, que será cobrado ao cliente
    public static float calcularCustoCliente(Veiculo veiculo, float distancia, float cargaTotal, float margemDeLucro){
        if (margemDeLucro<1){
            margemDeLucro = 1 - margemDeLucro;
        }
        return veiculo.entregarComMenosCusto(distancia, cargaTotal) / margemDeLucro;
    }

    // Método que retorna o custo benefício da viagem
    public static float calcularCustoBeneficio(Veiculo veiculo, float distancia, float cargaTotal){
        return veiculo.entregarCustoBeneficio(distancia, cargaTotal);
    }

    // Método para resetar os dados armazenados em arquivos
    public static int resetarDados(){
        // Declaração dos arquivos
        File arquivoVeiculos = new File("./doc/AcervoDeVeiculos.txt");
        File arquivoEntregas = new File("./doc/HistoricoDeViagem.txt");

        // Objetos de stream de escrita
        FileWriter  escritorPrimario;
        PrintWriter escritor;

        try{
            // Instanciamento do arquivo de veículos
            escritorPrimario  = new FileWriter(arquivoVeiculos);
            escritor          = new PrintWriter(escritorPrimario);

            // Sobrescrevendo o arquivo com os valores zerados
            escritor.println  ("VEICULOS: 0");
            escritor.println  ("#--#--# CARRETA #--#--#");
            escritor.println  ("disponível: 0");
            escritor.println  ("em_viagem: 0");
            escritor.println  ("#--#--# VAN #--#--#");
            escritor.println  ("disponível: 0");
            escritor.println  ("em_viagem: 0");
            escritor.println  ("#--#--# CARRO #--#--#");
            escritor.println  ("disponível: 0");
            escritor.println  ("em_viagem: 0");
            escritor.println  ("#--#--# MOTO #--#--#");
            escritor.println  ("disponível: 0");
            escritor.println  ("em_viagem: 0");

            // Fechando as streams
            escritor.close();
            escritorPrimario.close();

            // Instanciamento do arquivo de viagens
            escritorPrimario  = new FileWriter(arquivoEntregas);
            escritor          = new PrintWriter(escritorPrimario);

            // Sobrescrevendo o arquivo com os valores zerados
            escritor.println  ("Margem_De_Lucro: 0.2");
            escritor.println  ("Entregas_Realizadas: 0");
            escritor.println  ("Saldo: 0.0");
            escritor.println  ("#--#--#--#--#--#--#--#--#--#--#");

            // Fechando as streams
            escritor.close();
            escritorPrimario.close();

        }
        catch (Exception e){
            return 1;
        }

        return 0;
    }

}
