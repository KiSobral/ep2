public class Van extends Veiculo {
    // Construtor da classe Van
    public Van(){
        setNome("VAN");
        setCombustivel((byte)DIESEL);
        setRendimento(10.0f);
        setDiminuiRendimento(0.001f);
        setCargaMaxima(3500.0f);
        setVelocidadeMedia(80.0f);
        setPrecoCombustivel(getCombustivel());
    }

}
