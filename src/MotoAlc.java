public class MotoAlc extends Veiculo {
    // Construtor da classe MotoAlc
    public MotoAlc(){
        setNome("MOTO");
        setCombustivel((byte)ALCOOL);
        setRendimento(43.0f);
        setDiminuiRendimento(0.4f);
        setCargaMaxima(50.0f);
        setVelocidadeMedia(110.0f);
        setPrecoCombustivel(getCombustivel());
    }

}
