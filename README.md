# EP2 - OO 2019.1 (UnB - Gama)

## Transportadora

O programa desenvolvido para a EP2 tem como finalidade:
- Gerenciar a frota de uma transportadora
- Calcular as melhores opções de frete
- Persistir os dados de frota e de frete

Tendo isto em mente, o código foi produzido com o intuito de desenvolver uma interface gráfica para a simples utilização do software pelo usuário da transportadora.


## Veiculos
A transportadora tem um total de 4 veículos e suas respectivas características. Estes são:
- Carreta
- **Diesel como combustível**
- **30 toneladas de carga máxima**
- **8 KM/L de rendimento**
- **60Km/h como velocidade média**
- Van
  - **Diesel como combustível**
  - **10KM/L de rendimento**
  - **3.5 toneladas de carga máxima**
  - **80 Km/h como velocidade média**
- Carro
  - **Gasolina ou Álcool como combustível**
  - **14 Km/L de Rendimento**
  - **360 Kg de carga máxima**
  - **100 Km/h como Velocidade média**
- Moto
  - **Gasolina ou Álcool como combustível**
  - **50 Km/L de Rendimento**
  - **50 Kg de carga máxima**
  - **110 Km/h como Velocidade média**

Os valores dos combustíveis a serem utilizados são:
  - **Álcool**: R$ 3.499 por litro
  - **Gasolina**: R$ 4.449 por litro
  - **Diesel**: R$ 3.869 por litro


## Interface Gráfica
- Caso exista veículo que consiga realizar a entrega sem exceder os limites de peso e de tempo, deve ser possível selecioná-lo para o serviço;
  - O veículo disponível que possui o melhor custo benefício para a empresa, o tempo que levará para ele realizar a entrega e o custo de operação somado com a margem de lucro;
- Caso não exista veículo que consiga realizar a entrega sem exceder os limites de peso e de tempo, o usuário deve ser alertado e não pode ser possível selecionar veículos para o serviço;
- Os veículos selecionados para serviço não podem ser utilizados em outros fretes sem que o usuário os torne disponível novamente;
- A carga deve ser entregue em apenas uma viagem, por apenas um veículo;
